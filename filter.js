/**
 * filter array by given condition of callback
 * @param {*} arr - Given Array
 * @param {*} callback - Callback function
 */
function filter(arr, callback) {
  let res = [];
  if (typeof callback !== "function") return [];
  if (Array.isArray(arr)) {
    for (let i = 0; i < arr.length; i++) {
      if (callback(arr[i], i, arr)) res.push(arr[i]);
    }
    return res;
  } else {
    return [];
  }
}

module.exports.filter = filter;
