/**
 * map implemented
 * @param {*} arr - Array input
 * @param {*} callback - callback function
 */
function map(arr, callback) {
  let res = [];
  if (typeof callback !== "function") return [];
  if (Array.isArray(arr)) {
    for (let i = 0; i < arr.length; i++) {
      res.push(callback(arr[i], i, arr));
    }
    return res;
  }
}

module.exports.map = map;