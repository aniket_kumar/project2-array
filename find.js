/**
 * find if given present in array
 * @param {*} arr - Given Array
 * @param {*} callback - Callback function
 */
function find(arr, callback) {
  if (typeof callback !== "function") return undefined;
    if (Array.isArray(arr)) {
      for (let i = 0; i < arr.length; i++) {
        if (callback(arr[i], i, arr)) return true;
      }
    } else {
      return undefined;
    }
  return false;
}

module.exports.find=find;