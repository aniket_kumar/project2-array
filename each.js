/**
 * forEach implementation
 * @param {*} arr - required array
 * @param {*} callback -callback function
 */
function each(arr, callback) {
  if (typeof callback !== "function") {
    console.log("callback function not passed");
    return;
  }
  if (Array.isArray(arr)) {
    for (let i = 0; i < arr.length; i++) {
      callback(arr[i], i, arr);
    }
  } else {
    console.log("Its not array");
  }
}

module.exports.each = each;
