/**
 * Reduce function implemented
 * @param {*} arr - Array of element
 * @param {*} callback - callback function
 * @param {*} startValue - starting value
 */
function reduce(arr, callback, startValue) {
  if (startValue === undefined) startValue = arr[0];
  if (Array.isArray(arr) && typeof callback === "function") {
    for (let i = 0; i < arr.length; i++) {
      startValue = callback(startValue, arr[i]);
    }
    return startValue;
  } else return "Incorrect argument passed..";
}
module.exports.reduce = reduce;
