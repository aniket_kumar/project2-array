/**
 * this function return flatten array
 * @param {*} element 
 */
function flatten(element) {
  let res = [];
  if (Array.isArray(element)) {
    for (let i = 0; i < element.length; i++) {
      if (Array.isArray(element[i])) {
        res = res.concat(flatten(element[i]));
      } else {
        res.push(element[i]);
      }
    }
    return res;
  } else {
    return res;
  }
}
module.exports.flatten = flatten;
